import React, { Component } from 'react';
import './App.css';
import AddItems from "./Components/AddItems";
import OrderDetails from "./Components/OrderDetails";
import Food from './img/food.svg';
import Drink from './img/drink.svg';

class App extends Component {

    state = {
        items: [
            {name: 'Hamburger', price: 80, count: 0, image: Food},
            {name: 'Cheeseburger', price: 90, count: 0, image: Food},
            {name: 'Fries', price: 45, count: 0, image: Food},
            {name: 'Coffee', price: 70, count: 0, image: Drink},
            {name: 'Tea', price: 50, count: 0, image: Drink},
            {name: 'Cola', price: 40, count: 0, image: Drink},
        ],
        totalPrice: 0
    };

    addItemHandler = (point) => {
        let items = [...this.state.items];
        let id = items.findIndex(item => item.name === point.name);
        items[id].count = items[id].count + 1;

        let totalPrice = this.state.totalPrice;
        totalPrice += point.price;



        this.setState({items, totalPrice})
    };
    removeItemHandler = (point) => {
        let items = [...this.state.items];
        let id = items.findIndex(item => item.name === point.name);
        items[id].count = items[id].count - 1;

        let totalPrice = this.state.totalPrice;
        totalPrice -= point.price;

        this.setState({items, totalPrice})
    };

  render() {
    return (
      <div className="App">
          <OrderDetails items={this.state.items} rem={this.removeItemHandler} totalPrice={this.state.totalPrice} />
        <AddItems items={this.state.items} addClickHandler={this.addItemHandler} />
      </div>
    );
  }
}

export default App;
