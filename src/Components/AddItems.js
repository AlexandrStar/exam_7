import React from 'react';

const AddItems = props =>

    <div className="items"><h2>Add items:</h2>
        {props.items.map((item, key) =>
            <div key={key} className='wrapper'>
                <button onClick={() => props.addClickHandler(item)} className='btn-items'>
                    <img className='btn-items-img ' src={item.image} alt={item.name}/>
                    <h5>{item.name}</h5>
                    <p>Price: {item.price} KGS</p>
                </button>
            </div>)
        }
    </div>;


export default AddItems;