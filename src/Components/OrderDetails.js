import React from 'react';

const OrderDetails = props =>
    <div className="order"><h2>Order Details:</h2>
        {props.items.map((item, key) =>
            <div key={key} >
                {props.items[key].count > 0?
                <div className='point'>
                    <h5>{props.items[key].name}</h5>
                    <p>X - {props.items[key].count} </p>
                    <p>{item.price * props.items[key].count} KGS</p>
                    <button className='btn-remove' onClick={() => props.rem(item)}>удалить</button>
                </div>: null}
            </div>)
        }
        {props.totalPrice < 1? <p className='warning'>Order is empty! <br/> Please add some items!</p>:
            <p className='total-price'>Total price: {props.totalPrice} KGS</p>}
    </div>;

export default OrderDetails;